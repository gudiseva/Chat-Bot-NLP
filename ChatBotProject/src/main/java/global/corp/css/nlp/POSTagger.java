package global.corp.css.nlp;

import java.util.StringTokenizer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class POSTagger {
	
	Logger logger = LoggerFactory.getLogger(POSTagger.class);
	
	@SuppressWarnings("unchecked")
	public String texTagger(String text) {
		
		 // Initialize the tagger
		 MaxentTagger tagger = new MaxentTagger("taggers/english-left3words-distsim.tagger");
		 
		 //The tagged string
		 String taggedStr = tagger.tagString(text);
		 
		 //output the tagged sample string onto your console
		 logger.debug(taggedStr);
		 
		 StringTokenizer token = new StringTokenizer(taggedStr, " "); 
		 JSONObject obj = new JSONObject();
		 JSONArray vbList = new JSONArray();
		 JSONArray nnpList = new JSONArray();
			
			while (token.hasMoreTokens()) {
				
				String tempStr = token.nextToken();
				logger.debug(tempStr);
				
				if(tempStr.contains("_VB") && !tempStr.contains("_VBP")){
					String[] verbOutput = tempStr.split("_");
					logger.debug(verbOutput[1] + " : " + verbOutput[0]);
					vbList.add(verbOutput[0]);
				}
				obj.put("VB", vbList);
				
				if(tempStr.contains("_NNP")){
					String[] nounOutput = tempStr.split("_");
					logger.debug(nounOutput[1] + " : " + nounOutput[0]);
					nnpList.add(nounOutput[0]);
				}
				obj.put("NNP", nnpList);
			}
			
			logger.debug(obj.toJSONString());
			return obj.toJSONString();
	}

}
