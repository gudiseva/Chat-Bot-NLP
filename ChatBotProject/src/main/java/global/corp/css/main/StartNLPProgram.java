package global.corp.css.main;

import java.io.IOException;

import javax.jms.JMSException;
import javax.naming.NamingException;

import global.corp.css.pubsub.POSSubscriber;

public class StartNLPProgram {

	public static void main(String[] args) throws JMSException, NamingException, IOException {
		
		/*
		POSTagger posTagger = new POSTagger();
		String posStr = posTagger.texTagger("I want to change my Wi-Fi Router Password.");
		System.out.println(posStr);
		*/
		
		POSSubscriber posSubscriber = new POSSubscriber();
		posSubscriber.subscribe();
		//posSubscriber.publisher("Sample Text");
		
		/*
		CoreNLPRip coreNLPRip = new CoreNLPRip();
		coreNLPRip.ripper("I want to change my Wi-Fi Router Password.");
		*/
	}

}
