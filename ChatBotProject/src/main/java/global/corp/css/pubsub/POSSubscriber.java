package global.corp.css.pubsub;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import global.corp.css.nlp.POSTagger;

public class POSSubscriber {

	static Logger logger = LoggerFactory.getLogger(POSSubscriber.class);
	public static final String TOPIC = "topicChatBotTopic";
	
	static String activeMqUrl = ActiveMQConnection.DEFAULT_BROKER_URL;

	
	public void subscribe() throws JMSException, NamingException {

		Context sInitialContext = POSSubscriber.getInitialContext();
		Topic sTopic = (Topic) sInitialContext.lookup(POSSubscriber.TOPIC);
		
		// Set up the connection, same as the producer, however you also need to set a
		// unique ClientID which ActiveMQ uses to identify the durable subscriber
		TopicConnectionFactory sTopicConnectionFactory = (TopicConnectionFactory) sInitialContext.lookup("ConnectionFactory");
		TopicConnection sTopicConnection = sTopicConnectionFactory.createTopicConnection();
		sTopicConnection.setClientID("POSClientID");
		sTopicConnection.start();	

		
		// We don't want to use AUTO_ACKNOWLEDGE, instead we want to ensure the subscriber has successfully
		// processed the message before telling ActiveMQ to remove it, so we will use CLIENT_ACKNOWLEDGE
		Session sSession = sTopicConnection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
 
		// Register to be notified of all messages on the topic
		TopicSubscriber durableSubscriber = sSession.createDurableSubscriber(sTopic, "topicChatBotTopic");
 
		// Create a listener to process each received message
		MessageListener listener = new MessageListener() {
			
			public void onMessage(Message message) {

				POSTagger posTagger = new POSTagger();
				
		        try {
		        	String chatText = ((TextMessage) message).getText();
		        	logger.debug("Message received from Chat Client: '" + chatText + "'");
		        	
		        	int brac1Index = chatText.indexOf("[");
		        	logger.debug("Index of [ " + brac1Index);
		        	int colIndex = chatText.indexOf(":");
		        	logger.debug("Index of : " + colIndex);
		        	int brac2Index = chatText.lastIndexOf("]");
		        	logger.debug("Index of ] " + brac2Index);
		        	
		        	if (brac1Index >= 0 && colIndex >= 0 && brac2Index >= 0){
			        	String chatInitiator = chatText.substring(brac1Index + 1, colIndex);
			        	logger.debug("Message initiated by Chat Customer: '" + chatInitiator + "'");
			        	
			        	String prePOSText = chatText.substring(colIndex + 2, brac2Index);
			        	logger.debug("Message extracted from Chat Client: '" + prePOSText + "'");

			          	if (chatInitiator.equalsIgnoreCase("Bot")){
			          		logger.debug("Message initiated by Bot are ignored!");
			          	}
			          	else{
			          		String taggedText = posTagger.texTagger(prePOSText);
			          		logger.debug("Message converted in POS Tagger: '" + taggedText + "'");
				          	
			          		publisher(taggedText);
			          	}
			          	
		        	}
		        	else {
		        		logger.error("Message format not supported: '" + chatText + "'");
		        		publisher("Invalid Message Format!");
		        	}
		        	
		          	// Once we have successfully processed the message, send an acknowledge back to ActiveMQ
		          	message.acknowledge();

		          }
		          catch (JMSException je) {
		        	  logger.error(je.getMessage());
		          } catch (NamingException e) {
		        	  e.printStackTrace();
				}
			}
		};
	
		// Add the message listener to the durable subscriber
		durableSubscriber.setMessageListener(listener);
	    
	  }
	
	public void publisher(String pText) throws JMSException, NamingException {

		Context pInitialContext = POSSubscriber.getInitialContext();
		Topic pTopic = (Topic) pInitialContext.lookup(POSSubscriber.TOPIC);
		
		TopicConnectionFactory pTopicConnectionFactory = (TopicConnectionFactory) pInitialContext.lookup("ConnectionFactory");
		TopicConnection pTopicConnection = pTopicConnectionFactory.createTopicConnection();
		pTopicConnection.setClientID("POSBotID");
		pTopicConnection.start();	
		
		TopicSession pSession = pTopicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	    TextMessage pTextMessage = pSession.createTextMessage();
	    pTextMessage.setText("[Bot: " + pText + "]");
	    
	    //messageProducer.send(textMessage);
	    TopicPublisher pPublisher = pSession.createPublisher(pTopic);
	    pPublisher.publish(pTextMessage);
	    logger.debug("Message sent to subscriber: '" + pTextMessage.getText() + "'");
	    pTopicConnection.close();
	    
	  }
	
	
	
	public static Context getInitialContext() throws NamingException {
		Properties env = new Properties();
		
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		env.put(Context.PROVIDER_URL, "tcp://localhost:61616");
		//env.put(Context.PROVIDER_URL, "tcp://localhost:31313");
		env.put("topic.topicChatBotTopic", "ChatBotTopic");
		
		Context context = new InitialContext(env);
		return context;
	}

}
