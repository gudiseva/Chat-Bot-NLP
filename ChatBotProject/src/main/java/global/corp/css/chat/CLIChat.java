package global.corp.css.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CLIChat implements MessageListener {
	
	static Logger logger = LoggerFactory.getLogger(CLIChat.class);
	
	public static final String TOPIC = "topicChatBotTopic";
	static String activeMqUrl = ActiveMQConnection.DEFAULT_BROKER_URL;

	public static void main(String[] args) throws JMSException, NamingException, IOException {
		
		if (args.length != 1){
			System.out.println("Username is required");
		}
		else{
			String username = args[0];
			CLIChat cliChat = new CLIChat();
			cliChat.startChat(cliChat, username);			
		}
	}

	@Override
	public void onMessage(Message message) {
		try{
			System.out.println(((TextMessage) message).getText());
		}
		catch (Exception ex){
			logger.error("Error occured in onMessage() " + ex.getMessage(), ex);
		}
	}

	
	public void startChat(CLIChat cliChat, String username) {
		
		Context initialContext;
		try {
			initialContext = CLIChat.getInitialContext();
			Topic topic = (Topic) initialContext.lookup(CLIChat.TOPIC);
			TopicConnectionFactory topicConnectionFactory = (TopicConnectionFactory)initialContext.lookup("ConnectionFactory");
			TopicConnection topicConnection = topicConnectionFactory.createTopicConnection();
			cliChat.subscribe(topicConnection, topic, cliChat);
			cliChat.publish(topicConnection, topic, username);
			
		} catch (NamingException ex) {
			logger.error("Error occured in main() " + ex.getMessage(), ex);
		} catch (JMSException ex) {
			logger.error("Error occured in main() " + ex.getMessage(), ex);
		}
		
	}

	public void subscribe(TopicConnection topicConnection, Topic topic, CLIChat cliChat) {
		TopicSession subscribeSession;
		try {
			subscribeSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			TopicSubscriber topicSubscriber = subscribeSession.createSubscriber(topic);
			topicSubscriber.setMessageListener(cliChat);
		} catch (JMSException ex) {
			logger.error("Error occured in subscribe() " + ex.getMessage(), ex);
		}

	}
	
	public void publish(TopicConnection topicConnection, Topic topic, String username) {
		TopicSession publishSession;

		try {
			publishSession = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
			TopicPublisher topicPublisher = publishSession.createPublisher(topic);
			topicConnection.start();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			while(true){
				String messageToSend = reader.readLine();
				if(messageToSend.equalsIgnoreCase("exit")) {
					topicConnection.close();
					System.exit(0);
				}
				else {
					TextMessage message = publishSession.createTextMessage();
					message.setText("[" + username + ": " + messageToSend + "]");
					topicPublisher.publish(message);
				}
			}
		} catch (JMSException ex) {
			logger.error("Error occured in publish() " + ex.getMessage(), ex);
		} catch (IOException ex) {
			logger.error("Error occured in publish() " + ex.getMessage(), ex);
		}

	}
	
	public static Context getInitialContext() throws NamingException {
		Properties env = new Properties();
		
		env.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		env.put(Context.PROVIDER_URL, "tcp://localhost:61616");
		//env.put(Context.PROVIDER_URL, "tcp://localhost:31313");
		env.put("topic.topicChatBotTopic", "ChatBotTopic");
		
		Context context = new InitialContext(env);
		return context;
	}
	
}
